
#include "stdafx.h"
#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

int id;

/*
The function inserts the last id into global variable which is - id.
*/
int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	id = std::atoi(argv[0]);
	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");

	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name string)", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	rc = sqlite3_exec(db, "insert into people(name) values(\"name1\")", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	rc = sqlite3_exec(db, "insert into people(name) values(\"name2\")", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	rc = sqlite3_exec(db, "insert into people(name) values(\"name3\")", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	rc = sqlite3_exec(db, "select last_insert_rowid()", getLastId, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		char str[90] = "update people set name=\"name4\" where id=";
		strcat_s(str, (std::to_string(id)).c_str());
		rc = sqlite3_exec(db, str, NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "SQL error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}


	system("Pause");
	system("CLS");

	sqlite3_close(db);
	return 0;
}

